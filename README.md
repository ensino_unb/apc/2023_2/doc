# Plano da Disciplina - Algoritmos e Programação de Computadores (113476)

## Professores
* Fabricio Ataides Braz
* Nilton Correia da Silva

## Período
1º Semestre de 2.023

## Turmas
* 09 - Fabricio Ataides Braz
* 12 - Nilton Correia da Silva

## Ementa
Princípios fundamentais de construção de programas. Construção de algoritmos e sua representação em pseudocódigo e linguagens de alto nível. Noções de abstração. Especificação de variáveis e funções. Testes e depuração. Padrões de soluções em programação. Noções de programação estruturada. Identificadores e tipos. Operadores e expressões. Estruturas de controle: condicional e repetição. Entrada e saída de dados. Estruturas de dados estáticas: agregados homogêneos e heterogêneos. Iteração e recursão. Noções de análise de custo e complexidade. Desenvolvimento sistemático e implementação de programas. Estruturação, depuração, testes e documentação de programas. Resolução de problemas. Aplicações em casos reais e questões ambientais.

## Método

Independente do método de ensino, a programação de computador é uma habilidade, cuja apreensão exige experimentos contínuos de exercício dos fundamentos da programação. Várias abordagens servem ao própósito de motivar o aluno a buscar esse conhecimento. 

Nosso foco é a necessidade de estimular a turma a adquirir habilidades fundamentais para o desenvolvimento de algoritmos. Em razão disso, decidimos lançar mão da instrução no início do período letivo. Momento em que passaremos pela sensibilização sobre lógica de programação, usando para isso os recursos de [programação em blocos](https://code.org), e depois fundamentos de [programação em Python](https://github.com/PenseAllen/PensePython2e/).


## Ferramentas & Materiais
* [Aprender3](https://aprender3.unb.br/course/view.php?id=14603) - Comunicação e trabalho colaborativo
* [Python](https://www.python.org/) - Linguagem de programação
* [Code](https://studio.code.org/join/GDJXWQ) - Programação em blocos

**Ao se inscreverem no code informem em ``nome do usuário`` o seu email do aprender.unb.br**. Isso é para podermos consolidar as notas.

## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir uma média final (MF) superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Composição da Média Final (MF)

As atividades avaliativas individuais que compõem a MF do aluno são: 

* PE1, PE2, PE3: Provas Escritas.  

* ExCODE: nota correspondente ao percentual de exercícios resolvidos na plataforma code.org

* ExMoodle: nota correspondente ao percentual de exercícios resolvidos no próprio Moodle 

![equation](https://latex.codecogs.com/svg.image?NSeq&space;=&space;0,1*ExCODE&space;&plus;&space;0,2*ExMoodle;&plus;&space;0,7*PE1)

![equation](https://latex.codecogs.com/svg.image?NDec&space;=&space;0,2*ExMoodle&plus;0,8*PE2)

![equation](https://latex.codecogs.com/svg.image?NRep&space;=&space;0,2*ExMoodle&plus;0,8*PE3)

![equation](https://latex.codecogs.com/svg.image?MediaFinal&space;=&space;\frac{NSeq&space;&plus;2*NDec&space;&plus;&space;3*NRep}{6})



### Cronograma
| Aula | Data |Dia da Semana | Atividade|
|---|---|---|---|
|1|2023-03-28|Terça-feira|Acolhida|
|2|2023-03-30|Quinta-feira|Code.org|
|**3**|**2023-04-03**|**Segunda-feira**|**Code.org**|
|4|2023-04-04|Terça-feira|Code.org|
|5|2023-04-06|Quinta-feira|Code.org|
|**6**|**2023-04-10**|**Segunda-feira**|**[Introdução a sistema]()**|
|7|2023-04-11|Terça-feira|Code.org|
|8|2023-04-13|Quinta-feira|Code.org|
|**9**|**2023-04-17**|**Segunda-feira**|**[Introdução a programação](https://github.com/PenseAllen/PensePython2e/blob/master/docs/01-jornada.md)**|
|10|2023-04-18|Terça-feira|Prática de Programação|
|11|2023-04-20|Quinta-feira|Prática de Programação|
|**12**|**2023-04-24**|**Segunda-feira**|**[Variáveis e expressões](https://github.com/PenseAllen/PensePython2e/blob/master/docs/02-vars-expr-instr.md)**<br/>23:55 Prazo final para a entrega do Code.org|
|13|2023-04-25|Terça-feira|Prática de Programação|
|14|2023-04-27|Quinta-feira|Prática de Programação|
|-|2023-05-01|Segunda-feira|Feriado|
|15|2023-05-02|Terça-feira|Prática de Programação|
|16|2023-05-04|Quinta-feira|Prática de Programação|
|**17**|**2023-05-08**|**Segunda-feira**|**[Funções](https://github.com/PenseAllen/PensePython2e/blob/master/docs/03-funcoes.md)<br/>[Funções com Resultado](https://github.com/PenseAllen/PensePython2e/blob/master/docs/06-funcoes-result.md)**|
|18|2023-05-09|Terça-feira|Prática de Programação|
|19|2023-05-11|Quinta-feira|Prática de Programação|
|**20**|**2023-05-15**|**Segunda-feira**|**Prova Escrita 1 (PE1) - Estruturas Sequenciais <br/>**|
|21|2023-05-16|Terça-feira|Prática de Programação|
|22|2023-05-18|Quinta-feira|Prática de Programação|
|**23**|**2023-05-22**|**Segunda-feira**|**[Estrutura de Decisão](https://github.com/PenseAllen/PensePython2e/blob/master/docs/05-cond-recur.md)**|
|24|2023-05-23|Terça-feira|Prática de Programação|
|25|2023-05-25|Quinta-feira|Prática de Programação|
|**26**|**2023-05-29**|**Segunda-feira**|**[Estrutura de Decisão Aninhada](https://github.com/PenseAllen/PensePython2e/blob/master/docs/05-cond-recur.md)**|
|27|2023-05-30|Terça-feira|Prática de Programação|
|28|2023-06-01|Quinta-feira|Prática de Programação|
|**29**|**2023-06-05**|**Segunda-feira**|**Prova Escrita 2 (PE2) - Estruturas de Decisão**|
|30|2023-06-06|Terça-feira|Prática de Programação|
|-|2023-06-08|Quinta-feira|Feriado|
|**31**|**2023-06-12**|**Segunda-feira**|**[Estrutura de Repetição](https://github.com/PenseAllen/PensePython2e/blob/master/docs/07-iteracao.md)**|
|32|2023-06-13|Terça-feira|Prática de Programação|
|33|2023-06-15|Quinta-feira|Prática de Programação|
|**34**|**2023-06-19**|**Segunda-feira**|**[Listas](https://github.com/PenseAllen/PensePython2e/blob/master/docs/10-listas.md)**|
|35|2023-06-20|Terça-feira|Prática de Programação|
|36|2023-06-22|Quinta-feira|Prática de Programação|
|**37**|**2023-06-26**|**Segunda-feira**|**[Estrutura de Repetição](https://github.com/PenseAllen/PensePython2e/blob/master/docs/07-iteracao.md)**|
|38|2023-06-27|Terça-feira|Prática de Programação|
|39|2023-06-29|Quinta-feira|Prática de Programação|
|**40**|**2023-07-03**|**Segunda-feira**|**[Strings](https://github.com/PenseAllen/PensePython2e/blob/master/docs/08-strings.md)**|
|41|2023-07-04|Terça-feira|Prática de Programação|
|42|2023-07-06|Quinta-feira|Prática de Programação|
|**43**|**2023-07-10**|**Segunda-feira**|**Prova Escrita 3 (PE3) - Estruturas de Repetições**|
|44|2023-07-11|Terça-feira|Prática de Programação|
|45|2023-07-13|Quinta-feira|Revisao de Nota|


## Referências Bibliográficas

* Básica 
    * [Pense Python](https://github.com/PenseAllen/PensePython2e/tree/master/docs)
    * Cormen, T. et al., Algoritmos: Teoria e Prática. 3a ed., Elsevier - Campus, Rio de Janeiro, 2012 
    * Ziviani, N., Projeto de Algoritmos com implementação em Pascal e C, 3a ed., Cengage Learning, 2010. 
Felleisen, M. et al., How to design programs: an introduction to computing and programming, MIT Press, EUA, 2001. 

* Complementar 
    * Evans, D., Introduction to Computing: explorations in Language, Logic, and Machi nes, CreatSpace, 2011. 
    * Harel, D., Algorithmics: the spirit of computing, Addison-Wesley, 1978. 
    * Manber, U., Introduction to algorithms: a creative approach, Addison-Wesley, 1989. 
    * Kernighan, Brian W; Ritchie, Dennis M.,. C, a linguagem de programacao: Padrao ansi. Rio de janeiro: Campus 
    * Farrer, Harry. Programação estruturada de computadores: algoritmos estruturados. Rio de Janeiro: Guanabara Dois, 2002.
    * [Plotly](https://dash-gallery.plotly.host/Portal)
    * [Portal Brasileiro de Dados Abertos](http://dados.gov.br)
    * [Kaggle](http://kaggle.com/)
    * [Ambiente de Desenvolvimento Python](https://realpython.com/installing-python/)
    * [Python Basics](https://www.learnpython.org)

* Cursos de python
    - Os alunos podem escolher qualquer fonte extra de aprendendizagem para a linguagem Python. Os recursos são enormes. Algumas sugestões iniciais são:
    * https://www.pycursos.com/python-para-zumbis/
    * https://www.youtube.com/playlist?list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn
    * https://www.youtube.com/playlist?list=PLlrxD0HtieHhS8VzuMCfQD4uJ9yne1mE6
    * https://www.youtube.com/watch?v=O2xKiMl-d7Y&list=PL70CUfm2J_8SXFHovpVUbq8lB2JSuUXgk
    * https://solyd.com.br/treinamentos/python-basico/
    * https://wiki.python.org/moin/BeginnersGuide/NonProgrammers

